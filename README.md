# SpringBoot+MySQL+ReddisCache+CRUD+REST_APIs 

1st assignment :

API should perform CRUD using spring boot , mysql , redis

employee table can be used for CRUD operation .

used redis for employee & department mapping .

redis key < emp_id , department_name >

API input ( emp_id , emp_name )

emp_id should be check in redis to get the department_name .

If any employee id mapping not exists in redis then assign default department_name as ( default_dept )

department wise mysql db should be precreated with employee table .

employee table schema employee(id int, name string, department_name string )

department datasource object should be created once in entire lifetime .

department datasource object should be cached using spring memcache .

if another employee with same DB name comes via api we should refer the mem cache data source and not create db data source again.

for CRUD operation we can connect to db .

API should have validations on strict types and compulsory parameters and it should return proper error codes & error messages .

# REST-END-POINTS
```
To fetch all the employees present in the database -> GET => http://localhost:5030/api/employee/list
To create an employee object in the database -> POST => http://localhost:5030/api/employee/create
To fetch an employee with given id -> GET => http://localhost:5030/api/employee/read/{id}
To update an employee with given id -> PUT => http://localhost:5030/api/employee/update/{id}
To delete an employee with given id-> DELETE => http://localhost:5030/api/employee/delete/{id}
To fetch the department of an employee whose employee_id and employee_name is given -> POST => http://localhost:5030/api/employee/department
```
