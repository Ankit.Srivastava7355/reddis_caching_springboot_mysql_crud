package com.example.solution.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.solution.entity.Employee;



@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}