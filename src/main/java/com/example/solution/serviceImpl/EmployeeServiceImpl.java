package com.example.solution.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.example.solution.entity.Employee;
import com.example.solution.exception.EmployeeNotFoundException;
import com.example.solution.repository.EmployeeRepository;
import com.example.solution.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	private EmployeeRepository employeerepository;
	
	@Override
	public Employee CreateAnEmployee(Employee employee) {
		return employeerepository.save(employee);
	}

	@Override
	@CachePut(value="employee",key="#id")
	public Employee UpdateAnEmployee(Long id, Employee employee) throws EmployeeNotFoundException {
		Employee gg = employeerepository.findById(id).orElseThrow(()-> new EmployeeNotFoundException("Employee with give id not found"));
		gg.setEmp_name(employee.getEmp_name());
		gg.setDepartment_name(employee.getDepartment_name());
		employeerepository.save(gg);
		return gg;
	}

	@Override
	@Cacheable(value="employee",key="#id")
	public Employee GetAnEmployee(Long id) throws EmployeeNotFoundException {
		Employee gg = employeerepository.findById(id).orElseThrow(()-> new EmployeeNotFoundException("Employee with given id not found in database"));
		return gg;
	}

	@Override
	@CacheEvict(value="employee",key="#id")
	public void DeleteAnEmployee(Long id) throws EmployeeNotFoundException {
		Employee tt = employeerepository.findById(id).orElseThrow(()-> new EmployeeNotFoundException("Employee with given id not found in database"));
		employeerepository.deleteById(id);
	}

	@Override
	@Cacheable(value="employee",key="#emp_id")
	public Employee GetDepartmentOfEmployee(Long emp_id, String emp_name) throws EmployeeNotFoundException {
		System.out.println(emp_id);
		System.out.println(emp_name);
		Employee tt = employeerepository.findById(emp_id).orElseThrow(()-> new EmployeeNotFoundException("Employee with given id not found in database"));
		return tt;
	}

	@Override
	public List<Employee> GetAllEmployees() {
		return employeerepository.findAll();
	}

}
