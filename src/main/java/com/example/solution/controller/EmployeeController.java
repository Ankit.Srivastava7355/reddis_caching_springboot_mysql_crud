package com.example.solution.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.solution.entity.Employee;
import com.example.solution.request.DepartmentRequest;
import com.example.solution.response.ResponseHandler;
import com.example.solution.serviceImpl.EmployeeServiceImpl;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api")
public class EmployeeController {
	
	@Autowired
	private EmployeeServiceImpl employeeserviceimpl;
	
	// Get all the employees present in the database
	@GetMapping("/employee/list")
	public ResponseEntity<Object> listAllBooks() {
		try {
            return ResponseHandler.generateResponse("All Employee Data fetched Successfully!", HttpStatus.OK, employeeserviceimpl.GetAllEmployees());
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
	}
	
	// Create an employee
	@PostMapping("/employee/create")
	public ResponseEntity<Object> Create(@Validated @RequestBody Employee employee) {
		try {
            return ResponseHandler.generateResponse("Employee Created Successfully!", HttpStatus.OK, employeeserviceimpl.CreateAnEmployee(employee));
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
	}
	
	// Get employee with given id
	@GetMapping("/employee/read/{id}")
	public ResponseEntity<Object> Read(@PathVariable("id") Long id) {
		try {
            return ResponseHandler.generateResponse("Employee with given id fetched Successfully!", HttpStatus.OK, employeeserviceimpl.GetAnEmployee(id));
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
	}
	
	// Update employee with given id
	@PutMapping("/employee/update/{id}")
	public ResponseEntity<Object> Update(@PathVariable("id") Long id,@Valid @RequestBody Employee employee) {
		
		try {
            return ResponseHandler.generateResponse("Employee with given id Updated Successfully!", HttpStatus.OK, employeeserviceimpl.UpdateAnEmployee(id, employee));
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
	}
	
	// delete employee with given id
	@DeleteMapping("/employee/delete/{id}")
	public ResponseEntity<Object> Delete(@PathVariable("id") Long id) {
		try {
			employeeserviceimpl.DeleteAnEmployee(id);
            return ResponseHandler.generateResponse("Employee with given id Deleted Successfully!", HttpStatus.OK, "Deleted!");
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
	}
	
	@PostMapping("/employee/department")
	public ResponseEntity<Object> Department(@Valid @RequestBody DepartmentRequest department_details) {
		try {
		    return ResponseHandler.generateResponse("Department of given employee fetched Successfully!", HttpStatus.OK, employeeserviceimpl.GetDepartmentOfEmployee(department_details.getEmp_id(), department_details.getEmp_name()).getDepartment_name());
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
        }
	}

}
