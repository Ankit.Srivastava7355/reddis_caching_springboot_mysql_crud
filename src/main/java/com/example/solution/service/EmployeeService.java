package com.example.solution.service;

import java.util.List;

import com.example.solution.entity.Employee;
import com.example.solution.exception.EmployeeNotFoundException;

public interface EmployeeService {
	Employee CreateAnEmployee(Employee employee);
	Employee UpdateAnEmployee(Long id,Employee employee) throws EmployeeNotFoundException;
	Employee GetAnEmployee(Long id) throws EmployeeNotFoundException;
	void DeleteAnEmployee(Long id) throws EmployeeNotFoundException;
	Employee GetDepartmentOfEmployee(Long emp_id,String emp_name) throws EmployeeNotFoundException;
	List<Employee> GetAllEmployees();
}
